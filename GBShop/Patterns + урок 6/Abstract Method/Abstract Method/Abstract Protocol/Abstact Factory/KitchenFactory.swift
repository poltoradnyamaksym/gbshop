//
//  KitchenFactory.swift
//  Abstract Method
//
//  Created by Max Inedom on 9/22/21.
//

import Foundation

class KitchenFactory: AbstractFactory {
    func createChair() -> Chair {
        print("Стул для кухни")
        return ChairKitchen()
    }
    
    func createSofa() -> Sofa {
        print("Диван для кухни")
        return SofaKitchen()
    }
    
    func createtTable() -> Table {
        print("Стол для кухни")
        return TabkeKitchen()
    }
    
    
}
