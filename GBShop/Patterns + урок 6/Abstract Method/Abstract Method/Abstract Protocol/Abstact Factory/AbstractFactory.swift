//
//  AbstractFactory.swift
//  Abstract Method
//
//  Created by Max Inedom on 9/22/21.
//

import Foundation

protocol AbstractFactory {
    func createChair() -> Chair
    func createSofa() -> Sofa
    func createtTable() -> Table
}
