//
//  BedroomFacroty.swift
//  Abstract Method
//
//  Created by Max Inedom on 9/22/21.
//

import Foundation

class BedroomFactory: AbstractFactory {
    func createChair() -> Chair {
        print("Создался стул для спальни")
        return ChairBedroom()
    }
    
    func createSofa() -> Sofa {
        print("Создался диван для спальни")
        return SofaBedroom()
    }
    
    func createtTable() -> Table {
        print("Создался стол для спальни")
        return CoffeeTableBedroom()
    }
}
