//
//  ViewController.swift
//  Abstract Method
//
//  Created by Max Inedom on 9/22/21.
//

import UIKit

class ViewController: UIViewController {

    var chair: Chair?
    var table: Table?
    var sofa: Sofa?
    
    @IBAction func kitechenOrder(_ sender: Any) {
        chair = KitchenFactory().createChair()
        table = KitchenFactory().createtTable()
        sofa = KitchenFactory().createSofa()
    }
    
    @IBAction func bedroomaOrder(_ sender: Any) {
        chair = BedroomFactory().createChair()
        table = BedroomFactory().createtTable()
        sofa = BedroomFactory().createSofa()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

