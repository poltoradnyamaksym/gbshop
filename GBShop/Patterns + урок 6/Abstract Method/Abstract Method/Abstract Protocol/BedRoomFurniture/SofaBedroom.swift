//
//  SofaBedroom.swift
//  Abstract Method
//
//  Created by Max Inedom on 9/22/21.
//

import Foundation

class SofaBedroom: Sofa {
    var name: String = "Диван"
    var type: String = "Стол для спальни"
}
