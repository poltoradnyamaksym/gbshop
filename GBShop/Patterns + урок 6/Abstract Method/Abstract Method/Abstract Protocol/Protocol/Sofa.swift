//
//  Sofa.swift
//  Abstract Method
//
//  Created by Max Inedom on 9/22/21.
//

import Foundation

protocol Sofa {
    var name: String {get}
    var type: String {get}
}
