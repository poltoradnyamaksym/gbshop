//
//  Chair.swift
//  Abstract Method
//
//  Created by Max Inedom on 9/22/21.
//

import Foundation

protocol Chair {
    var name: String {get}
    var type: String {get}
}
