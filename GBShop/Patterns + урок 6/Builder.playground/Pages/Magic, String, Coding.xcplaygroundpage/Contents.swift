//: A UIKit based Playground for presenting user interface
  
import UIKit
import Foundation
import PlaygroundSupport

var one: Int = 1
var two: UInt8 = 3


func sum(one: inout Int, two: inout Int) -> Int{
    let sum = one + two
    return sum
}

//sum(one: &one, two: &two)


//MARK: Типы данных с плавающей точкой

func someFunc() {
    var result = parse(data: "1,2,3,4")
    print(result)
    
    result = parse(data: "a;b;c;d")
    print(result)
}

func parse(data: String) -> [String] {
    
    let separator = [";", ",", ":"]
    
    for sep in separator {
        if data.range(of: sep) != nil {
            return data.components(separatedBy: sep)
        }
    }
    return [String]()
}


let buffer = "Какой-то текст"

if let data = buffer.data(using: .utf8) {

/*
 Манипуляции с кодом
 В котором мы забыли о кодировке
 */

    let endBuffer = String(data: data, encoding: .ascii)
    
    print("Buffer: \(buffer)")
    print("Encoding buffer \(endBuffer ?? "")")

//Buffer: Какой-то текст
//Encoding buffer: 
    
    
    
    





