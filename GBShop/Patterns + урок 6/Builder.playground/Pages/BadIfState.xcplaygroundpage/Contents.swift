//: [Previous](@previous)

import Foundation

enum DefaultAuthData {
    static let login = "login"
    static let password = "password"
}


func auth(login: String, password: String) -> Bool {
    var isValidLogin = login == DefaultAuthData.login
    var isValidPassword = password == DefaultAuthData.password
    if isValidLogin && isValidPassword {
        return true
    } else {
        return false
    }
}
