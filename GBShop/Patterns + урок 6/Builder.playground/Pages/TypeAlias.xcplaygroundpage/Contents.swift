//: [Previous](@previous)

import Foundation
import UIKit
import PlaygroundSupport



typealias Buffer = String
typealias Separator = String
typealias SeparatorHeight = CGFloat
typealias Data = String
typealias IngnoreValue = String
typealias IsUpper = Bool


func parse(data: Data, separator: Separator, ignoreValue: IngnoreValue, isUpper: IsUpper) -> [String] {
    var result = [String]()
    if data.range(of: separator) != nil {
        for item in data.components(separatedBy: separator) {
            if item != ignoreValue {
                result.append(isUpper ? item.uppercased() : item)
            }
        }
    }
    return result
}

func configure(closure: (Data, Separator, IngnoreValue, IsUpper) -> [String]) {
    
}


func someFunc() {
    let result = parse(data: "a,b,c,d", separator: ",", ignoreValue: "a", isUpper: true)
    print(result)
}

someFunc()


typealias Result = String
typealias ResultCode = String
typealias Complete = (Result,ResultCode) -> ()


func sendRequest(url: String,data: String, completionHandler: Complete) {
    
}
sendRequest(url: "", data: "", completionHandler: {
    statusCode, result in
    
})

sendRequest(url: "", data: "") { statusCode, result in
    
}


//MARK: - паттерн репозиторий

protocol DataBase {
    associatedtype Entity
    
    func get(id: Int) -> Entity
    func add(object: Entity)
    func remove(id: Int)
}


class Book {
    var id: Int
    
    init(id: Int) {
        self.id = id
    }
}

class CoreDataBook: DataBase {
    typealias Entity = Book
    
    func get(id: Int) -> Book {
        return Entity(id: id)
    }
    
    func add(object: Book) {}
    
    func remove(id: Int) {}
}

class RealmBook: DataBase {
    typealias Entity = Book
    
    func get(id: Int) -> Book {
        return Entity(id: id)
    }
    
    func add(object: Book) {}
    
    func remove(id: Int) {}
}


class DataPresenter<DabaseEntity: DataBase> {
    var bookDatabase: DabaseEntity
    
    init(bookDatabase: DabaseEntity) {
        self.bookDatabase = bookDatabase
    }
    
    func myBook() -> DabaseEntity.Entity {
        return bookDatabase.get(id: 2)
    }
}


let presenterCoreData = DataPresenter<CoreDataBook>(bookDatabase: CoreDataBook())
let presenterRealm = DataPresenter<RealmBook>(bookDatabase: RealmBook())




