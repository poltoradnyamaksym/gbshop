//: [Previous](@previous)

import Foundation

func parse(data: String) -> [String] {
    let separators = [";", ":", ","]
    
    for separator in separators {
        let isExistsSepator = data.range(of: separator) != nil
            
        if isExistsSepator {
            return data.components(separatedBy: separator)
        }
    }
    return [String]()
}
