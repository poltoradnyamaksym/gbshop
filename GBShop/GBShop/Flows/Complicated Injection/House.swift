//
//  House.swift
//  GBShop
//
//  Created by Max Inedom on 9/18/21.
//

import Foundation

class House {
    
    private let family: Family?
    
    init(family: Family?) {
        self.family = family
    }
    
    func familyInHouse() -> String {
        return "family: \(family?.info() ?? "")"
    }
}
