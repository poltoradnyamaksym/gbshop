//
//  HouseNumber.swift
//  GBShop
//
//  Created by Max Inedom on 9/18/21.
//

import Foundation

final class HouseNumber: Family {
    
    private let number: Int
    
    init(number: Int) {
        self.number = number
    }
    
    func info() -> String {
        return "house number \(number)"
    }
}
