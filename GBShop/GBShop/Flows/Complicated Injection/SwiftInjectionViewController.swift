//
//  SwiftInjectionViewController.swift
//  GBShop
//
//  Created by Max Inedom on 9/18/21.
//

import UIKit
import Swinject

class SwiftInjectionViewController: UIViewController {
    
    
    private let container = Container()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        container.register(Family.self) { _ in Adress(familyAdress: "3-я улица Строителей, 25, кв. 12")}
        container.register(House.self) { i in
            House(family: i.resolve(Family.self))
        }
        
        let house = container.resolve(House.self)
    }
}

