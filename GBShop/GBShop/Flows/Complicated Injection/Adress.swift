//
//  Adress.swift
//  GBShop
//
//  Created by Max Inedom on 9/18/21.
//

import Foundation

class Adress: Family {

    private let familyAdress: String
    
    init(familyAdress: String) {
        self.familyAdress = familyAdress
    }
    
    func info() -> String {
        return "Adress name: \(familyAdress)"
    }
}
