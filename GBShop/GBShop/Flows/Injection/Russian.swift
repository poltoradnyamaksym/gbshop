//
//  Russian.swift
//  GBShop
//
//  Created by Max Inedom on 9/18/21.
//

import Foundation

final class Russian: Nationality {
    
    let nationality: String
    
    init(nationality: String) {
        self.nationality = nationality
    }
    
    func nation() -> String {
        return "Passport nationality: \(nationality)"
    }
}
