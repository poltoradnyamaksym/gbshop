//
//  InjectionViewController.swift
//  GBShop
//
//  Created by Max Inedom on 9/18/21.
//

import UIKit

final class InjectionViewController: UIViewController {

    private let passportOwner = Passport(nationality: American(nationality: "American"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
