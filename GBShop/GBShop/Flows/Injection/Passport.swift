//
//  Person.swift
//  GBShop
//
//  Created by Max Inedom on 9/18/21.
//

import Foundation

final class Passport {
    
    private let nationality: Nationality
    
    init(nationality: Nationality) {
        self.nationality = nationality
    }
    
    func passportOnwer() -> String {
        return "Passport owner \(nationality.nation())"
    }
    
}
