//
//  Family.swift
//  GBShop
//
//  Created by Max Inedom on 9/18/21.
//

import Foundation

protocol Family {
    func info() -> String
}
