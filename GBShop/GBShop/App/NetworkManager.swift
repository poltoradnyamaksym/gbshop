//
//  NetworkManager.swift
//  GBShop
//
//  Created by Max Inedom on 9/21/21.
//

import Foundation

enum NetworkResult {
    case suceess
    case error
}

class NetworkManager {
    
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    
    func getData(url: URL, completion: @escaping (NetworkResult) -> ()) {
        let task = session.dataTask(with: url) {
            data, responce, error in
            if let data = data {
                completion(.suceess)
            } else {
                completion(.error)
            }
        }
        task.resume()
    }
}
