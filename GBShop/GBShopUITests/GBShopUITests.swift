//
//  GBShopUITests.swift
//  GBShopUITests
//
//  Created by Max Inedom on 9/21/21.
//

import XCTest

class GBShopUITests: XCTestCase {

    //btnShop, txtShop
    
    var app: XCUIApplication?
    
    override func setUpWithError() throws {

        continueAfterFailure = true

        app = XCUIApplication()
        app?.launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        app?.buttons["btnShop"].tap()
        app?.waitForExistence(timeout: 2.0)
        app?.textFields["txtShop"].typeText("default text")
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
