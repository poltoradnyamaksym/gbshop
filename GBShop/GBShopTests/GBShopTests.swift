//
//  GBShopTests.swift
//  GBShopTests
//
//  Created by Max Inedom on 9/21/21.
//

import XCTest
import Foundation
@testable import GBShop


class URLSessionDataTaskMock: URLSessionDataTask {
    private let closure: () -> Void
    
    init(closure: @escaping () -> ()) {
        self.closure = closure
    }
    
    override func resume() {
        closure()
    }
}

class URLSessionMock: URLSession  {
    var data: Data?
    var error: Error?
    
    override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let data = self.data
        let error = self.error
        
        return URLSessionDataTaskMock { completionHandler(data, nil, error) }
    }
}

class GBShopTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testNetworkRequestError() {
        let session = URLSessionMock()
        let manager = NetworkManager(session: session)
        
        let data = Data(bytes: [1, 0, 1])
        session.data = data
        
        let url = URL(fileURLWithPath: "default")
        var result: NetworkResult?
        
        manager.getData(url: url, completion: { result = $0 })
        
        XCTAssertEqual(result, .suceess)
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
}
